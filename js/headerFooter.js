$(function(){
	$(".header").html('<div class="headerNavigation">'
							   	+'<div class="wrap hn_main">'
							   		+'<div class="hn_logo">'
						           		+'<a href="index.html"><img  src="images/logo.png"/></a>'
						           	+'</div>'
						           	+'<div class="hn_menu">'
							           	+'<ul>'
							           		+'<li><a  id="shouye" href="index.html">首页</a></li>'
									        +'<li><a  id="style"    href="musicStyle.html">音乐风格</a></li>'
									        +'<li><a  id="member"  href="member.html">会员中心</a></li>'
									        +'<li><a  id="aboutUs"  href="aboutUs.html">关于我们</a></li>'
							            +'</ul> '
						            +'</div>'
						            +'<div class="hn_right">'
							           	+'<div class="mod_top_search">'
							           		+'<div class="mod_search_input">'
									            +'<input class="search_input__input" type="text" placeholder="因为刚好遇见你" accesskey="s">'
									            +'<button class="search_input__btn"><i class="icon_search"></i><span class="icon_txt">搜索</span></button>'
							                +'</div> '
							            +'</div> '
							            +'<p class="msg">'
							                +'<a href="javascript:;" id="login" style="margin-right:14px;">登录</a>'
							                +'<a href="registered.html" id="register">注册</a>'							           
							           +'</p>'
						            +'</div>'
						        +'</div>'
						    +'</div>'
	);
	$(".Footer").html('<div class="footer">'
							+'<div class="wrap">'
								+'<p>© 2017 北京左手网络科技有限公司  京ICP备15032703号</p>'
							+'</div>'
						+'</div>'
	 );	
	 
	$("#login").on("click",function(){
		$(".meng").show();
		$(".new-bg-nologin .imgbox").show();
	});
});
